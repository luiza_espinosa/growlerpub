package org.webi.growlerpub;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GrowlerPubApp {

	public static void main(String[] args) {
		SpringApplication.run(GrowlerPubApp.class, args);
	}

}
