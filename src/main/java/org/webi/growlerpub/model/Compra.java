package org.webi.growlerpub.model;

import java.time.LocalDate;
import java.util.ArrayList;

public class Compra {
    private int id;
    private float valor_total = 0;
    private LocalDate data_compra;
    private ArrayList<Produto> items = new ArrayList<>();
    private Cliente cliente;
    private Funcionario funcionario;
    private boolean pago;

    public LocalDate getData_compra() {
        return data_compra;
    }

    public void setData_compra(LocalDate data_compra) {
        this.data_compra = data_compra;
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public boolean isPago() {
        return pago;
    }

    public void setPago(boolean pago) {
        this.pago = pago;
    }

    public float getValor_total() {
        return valor_total;
    }

    public void setValor_total(float valor_total) {
        this.valor_total += valor_total;
    }

    public ArrayList<Produto> getItems() {
        return items;
    }

    public void setItems(ArrayList<Produto> items) {
        this.items = items;
    }

    public Funcionario getFuncionario() {
        return funcionario;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public void setFuncionario(Funcionario funcionario) {
        this.funcionario = funcionario;
    }

    @Override
    public String toString() {
        return "";
    }
}
