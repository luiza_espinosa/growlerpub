package org.webi.growlerpub.service;

import org.webi.growlerpub.dao.FuncionarioDAO;
import org.webi.growlerpub.model.Funcionario;

import java.util.ArrayList;

public class FuncionarioService {

    public Funcionario autenticar (String cpf, String matricula) {
        Funcionario u = new FuncionarioDAO().buscaFuncionario(cpf);
        if (u != null) {
            if (u.getMatricula() != null && u.getMatricula().equals(matricula)){
                return u;
            }
        }
        return null;
    }

    public void editarFuncionario(Funcionario u){
        new FuncionarioDAO().editarFuncionario(u);
    }

    public Funcionario buscarFuncionarioPeloId(int id){
        return new FuncionarioDAO().buscarFuncionarioPeloId(id);
    }

    public ArrayList<Funcionario> buscarTodosFuncionarios(){
        return new FuncionarioDAO().buscarTodosFuncionarios();
    }

    public void cadastraFuncionario(Funcionario funcionario) {
        new FuncionarioDAO().cadastrarFuncionario(funcionario);
    }

    public void excluirFuncionario(int id) {
        new FuncionarioDAO().deletarFuncionario(id);
    }
}
