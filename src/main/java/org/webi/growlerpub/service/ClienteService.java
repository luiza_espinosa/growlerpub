package org.webi.growlerpub.service;

import org.webi.growlerpub.dao.ClienteDAO;
import org.webi.growlerpub.model.Cliente;

import java.util.ArrayList;

public class ClienteService {
    public ArrayList<Cliente> buscarClientes(){
        return new ClienteDAO().buscarClientes();
    }

    public void cadastrarClientes(Cliente cliente) {
        if(!new ClienteDAO().buscarClientePorCpf(cliente.getCpf())){
            new ClienteDAO().cadastrarClientes(cliente);
        }
    }

    public boolean editarCliente(Cliente cliente){
        return new ClienteDAO().editarCliente(cliente);
    }

    public Cliente buscarClientePorId(int id_cliente) {
        return new ClienteDAO().buscarClientePeloId(id_cliente);
    }

    public void excluirCliente(int id_cliente) {
        new ClienteDAO().excluirCliente(id_cliente);
    }
}
