package org.webi.growlerpub.service;

import org.webi.growlerpub.dao.CompraDAO;
import org.webi.growlerpub.model.Compra;
import org.webi.growlerpub.model.Produto;

import java.time.LocalDate;
import java.util.ArrayList;

public class CompraService {
    public void cadastrarCompra (Compra compra) {
        compra.setData_compra(LocalDate.now());
        ArrayList<Produto> valores = new ProdutosService().buscarTodosProdutos();
        compra.getItems().forEach(item -> {
            valores.stream()
                    .filter(m -> m.getId() == item.getId())
                    .findFirst()
                    .ifPresent(m -> item.setValor(m.getValor() * item.getQuantidade()));
        });

        double valorTotal = compra.getItems().stream()
                .mapToDouble(Produto::getValor)
                .sum();

        compra.setValor_total((float)valorTotal);
        new CompraDAO().cadastrarCompra(compra);
    }
    public ArrayList<Compra> getHistoricoUsuario (int id){
        return new CompraDAO().buscarComprasDoCliente(id);
    }
    public ArrayList<Compra> todasAsCompras () { return new CompraDAO().todasAsCompras();}

    public Compra buscarCompraPeloIdCompra(int id) {
        return new CompraDAO().buscarCompraPeloIdCompra(id);
    }

    public void editarCompra(Compra compra) {
        compra.setData_compra(LocalDate.now());
        ArrayList<Produto> valores = new ProdutosService().buscarTodosProdutos();
        compra.getItems().forEach(item -> {
            valores.stream()
                    .filter(m -> m.getId() == item.getId())
                    .findFirst()
                    .ifPresent(m -> item.setValor(m.getValor() * item.getQuantidade()));
        });

        double valorTotal = compra.getItems().stream()
                .mapToDouble(Produto::getValor)
                .sum();

        compra.setValor_total((float)valorTotal);
        new CompraDAO().editarCompra(compra);
    }

    public void efetuarPagamento(Compra compra) {
        new CompraDAO().pagarCompra(compra);
    }
}
