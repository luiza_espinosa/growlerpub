package org.webi.growlerpub.service;

import org.webi.growlerpub.dao.ProdutoDAO;
import org.webi.growlerpub.model.Produto;

import java.util.ArrayList;

public class ProdutosService {
    public ArrayList<Produto> buscarTodosProdutos(){
        return new ProdutoDAO().buscarTodosProdutos();
    }

    public Produto buscarProdutoPeloId(int id_produto) {
        return new ProdutoDAO().buscaProduto(id_produto);
    }

    public void cadastrarProduto(Produto produto) {
        new ProdutoDAO().cadastrarProduto(produto);
    }

    public void editarProduto(Produto produto) {
        new ProdutoDAO().editarProduto(produto);
    }
}
