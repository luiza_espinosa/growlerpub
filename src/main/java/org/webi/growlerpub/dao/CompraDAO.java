package org.webi.growlerpub.dao;

import org.webi.growlerpub.model.Cliente;
import org.webi.growlerpub.model.Compra;
import org.webi.growlerpub.model.Produto;
import org.webi.growlerpub.model.Funcionario;

import java.sql.*;
import java.util.ArrayList;

public class CompraDAO {
    private String sql;
    private ResultSet rs;
    private PreparedStatement preparedStatement;
    private Connection conn;

    public boolean cadastrarCompra(Compra compra){
        try{
            this.conn = new ConectaDB().getConexao();
            this.sql = "INSERT INTO compra(id_cliente, id_funcionario, pago, data_compra) VALUES (?,?,?,?)";

            this.preparedStatement = this.conn.prepareStatement(this.sql, PreparedStatement.RETURN_GENERATED_KEYS);
            this.preparedStatement.setInt(1, compra.getCliente().getId());
            this.preparedStatement.setFloat(2, compra.getFuncionario().getId());
            this.preparedStatement.setBoolean(3, compra.isPago());
            this.preparedStatement.setDate(4,  Date.valueOf(compra.getData_compra()));
            this.preparedStatement.execute();
            this.rs = this.preparedStatement.getGeneratedKeys();
            this.rs.next();

            if(this.rs.getInt(1) > 0){
                compra.setId(this.rs.getInt(1));
                float valorTot = 0;
                for (Produto item : compra.getItems()) {
                    this.sql = "INSERT INTO item(id_compra, id_produto, valor, quantidade) VALUES (?, ?, ?, ?);";
                    this.preparedStatement = this.conn.prepareStatement(sql);
                    this.preparedStatement.setInt(1, compra.getId());
                    this.preparedStatement.setInt(2, item.getId());
                    this.preparedStatement.setFloat(3, item.getValor() * item.getQuantidade());
                    this.preparedStatement.setInt(4, item.getQuantidade());
                    this.preparedStatement.execute();

                    valorTot+= item.getValor();
                }
                this.sql = "UPDATE compra SET valor_total=? WHERE id_compra=?";
                this.preparedStatement = this.conn.prepareStatement(sql);
                this.preparedStatement.setFloat(1, valorTot);
                this.preparedStatement.setInt(2, compra.getId());
                this.preparedStatement.execute();
            }
        }catch (SQLException e){
            e.printStackTrace();
            return false;
        }
        return true;
    }
    public ArrayList<Compra> buscarComprasDoCliente(int id){
        ArrayList<Compra> compras = new ArrayList<>();

        try{
            this.conn = new ConectaDB().getConexao();
            this.sql = "SELECT * FROM compra WHERE id_cliente = ?";

            this.preparedStatement = this.conn.prepareStatement(this.sql);
            this.preparedStatement.setInt(1,id);
            this.rs = this.preparedStatement.executeQuery();

            while(this.rs.next()){
                Compra c = new Compra();
                c.setId(rs.getInt("id_compra"));
                c.setValor_total(rs.getFloat("valor_total"));
                compras.add(c);
            }
        }catch (SQLException e){
            e.printStackTrace();
            return null;
        }
        return compras;
    }

    public Compra buscarCompraPeloIdCompra(int id){
        Compra c = new Compra();
        try {
            this.conn = new ConectaDB().getConexao();
            this.sql = "SELECT c.*, f.nome , cli.nome, p.nome, p.valor, i.quantidade, i.id_produto, cli.cpf  " +
                    "FROM compra c JOIN funcionario f ON c.id_funcionario = f.id_funcionario JOIN cliente cli " +
                    "ON cli.id_cliente = c.id_cliente JOIN item i ON i.id_compra = c.id_compra JOIN produto p " +
                    "ON p.id_produto = i.id_produto AND c.id_compra=?;";
            this.preparedStatement = this.conn.prepareStatement(this.sql);

            this.preparedStatement.setInt(1,id);
            this.rs = this.preparedStatement.executeQuery();
            ArrayList<Produto> items = new ArrayList<>();
            if(this.rs.next()){
                c.setId(rs.getInt("id_compra"));
                c.setValor_total(rs.getFloat("valor_total"));

                Cliente cli = new Cliente();
                cli.setId(this.rs.getInt("id_cliente"));
                cli.setCpf(this.rs.getString(13));
                cli.setNome(this.rs.getString(8));
                c.setCliente(cli);

                Funcionario func = new Funcionario();
                func.setId(this.rs.getInt("id_funcionario"));
                func.setNome(this.rs.getString(7));
                c.setFuncionario(func);

                Produto prodIni = new Produto();
                prodIni.setNome(this.rs.getString(9));
                prodIni.setValor(this.rs.getFloat("valor"));
                prodIni.setQuantidade(this.rs.getInt(11));
                prodIni.setId(this.rs.getInt(12));
                items.add(prodIni);
                while(this.rs.next()){
                    Produto prod = new Produto();
                    prod.setNome(this.rs.getString(9));
                    prod.setValor(this.rs.getFloat("valor"));
                    prod.setQuantidade(this.rs.getInt(11));
                    prod.setId(this.rs.getInt(12));
                    items.add(prod);
                }
            }
            c.setItems(items);
        }catch (SQLException e){
            e.printStackTrace();
            return null;
        }
        return c;
    }

    public ArrayList<Compra> todasAsCompras(){
        ArrayList<Compra> compras = new ArrayList<>();
        try{
            this.conn = new ConectaDB().getConexao();
            this.sql = "SELECT id_compra, valor_total, data_compra, pago, cli.nome as nome_cli FROM compra c JOIN cliente cli " +
                    " ON cli.id_cliente = c.id_cliente;";
            this.preparedStatement = this.conn.prepareStatement(this.sql);

            this.rs = this.preparedStatement.executeQuery();

            while(rs.next()){
                Compra c = new Compra();
                c.setId(this.rs.getInt("id_compra"));
                c.setValor_total(this.rs.getFloat("valor_total"));
                c.setData_compra(this.rs.getDate("data_compra").toLocalDate());
                c.setPago(this.rs.getBoolean("pago"));

                Cliente cliente = new Cliente();
                cliente.setNome(this.rs.getString("nome_cli"));
                c.setCliente(cliente);

                compras.add(c);
            }
        } catch (SQLException e){
            e.printStackTrace();
            return null;
        }
        return compras;
    }

    public void editarCompra(Compra compra) {
        try{
            this.conn = new ConectaDB().getConexao();
            this.sql = "UPDATE compra SET valor_total=? WHERE id_compra=?";

            this.preparedStatement = this.conn.prepareStatement(this.sql);
            this.preparedStatement.setFloat(1, compra.getValor_total());
            this.preparedStatement.setFloat(2, compra.getId());
            this.preparedStatement.execute();

            if(compra.getId()>0){
                this.sql = "DELETE FROM item WHERE id_compra=?;";
                this.preparedStatement = this.conn.prepareStatement(sql);
                this.preparedStatement.setInt(1, compra.getId());
                this.preparedStatement.execute();

                for (Produto item : compra.getItems()) {
                    this.sql = "INSERT INTO item(id_compra, id_produto, valor, quantidade) VALUES (?, ?, ?, ?);";
                    this.preparedStatement = this.conn.prepareStatement(sql);
                    this.preparedStatement.setInt(1, compra.getId());
                    this.preparedStatement.setInt(2, item.getId());
                    this.preparedStatement.setFloat(3, item.getValor() * item.getQuantidade());
                    this.preparedStatement.setInt(4, item.getQuantidade());
                    this.preparedStatement.execute();
                }
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    public void pagarCompra(Compra compra){
        try {
            this.conn = new ConectaDB().getConexao();
            this.sql = "UPDATE compra SET pago=true WHERE id_compra=?";
            this.preparedStatement = conn.prepareStatement(this.sql);
            this.preparedStatement.setInt(1, compra.getId());
            this.preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
