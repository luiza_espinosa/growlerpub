package org.webi.growlerpub.dao;

import org.webi.growlerpub.model.Funcionario;

import java.sql.*;
import java.util.ArrayList;

public class FuncionarioDAO {
    private String sql;
    private ResultSet rs;
    private PreparedStatement preparedStatement;
        private Connection conn;

    public ArrayList<Funcionario> buscarTodosFuncionarios(){
        ArrayList<Funcionario> funcionarios = new ArrayList<Funcionario>();

        try(Connection conn = new ConectaDB().getConexao()){
            this.sql = "SELECT * FROM funcionario ORDER BY nome";
            this.preparedStatement = conn.prepareStatement(sql);
            this.rs = preparedStatement.executeQuery();

            while(this.rs.next()){
                Funcionario user = new Funcionario();
                user.setId(this.rs.getInt("id_funcionario"));
                user.setNome(this.rs.getString("nome"));
                user.setCpf(this.rs.getString("cpf"));
                user.setTelefone(this.rs.getString("telefone"));
                user.setMatricula(this.rs.getString("matricula"));
                user.setAtivo(this.rs.getBoolean("ativo"));
                funcionarios.add(user);
            }
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
        return funcionarios;
    }
    public Funcionario buscaFuncionario(String cpf){
        Funcionario u = new Funcionario();
        try {
            this.conn = new ConectaDB().getConexao();
            this.sql = "SELECT * FROM funcionario WHERE cpf=?";
            this.preparedStatement = this.conn.prepareStatement(this.sql);
            preparedStatement.setString(1,cpf);
            this.rs = this.preparedStatement.executeQuery();

            while(this.rs.next()){
                u.setId(this.rs.getInt("id_funcionario"));
                u.setNome(this.rs.getString("nome"));
                u.setCpf(this.rs.getString("cpf"));
                u.setMatricula(this.rs.getString("matricula"));
                u.setTelefone(this.rs.getString("telefone"));
            }
        }catch(SQLException e){
            e.printStackTrace();
            return null;
        }
        return u;
    }

    public Funcionario buscarFuncionarioPeloId(int id){
        Funcionario u = new Funcionario();
        try {
            this.conn = new ConectaDB().getConexao();
            this.sql = "SELECT * FROM funcionario WHERE id_funcionario=?";
            this.preparedStatement = this.conn.prepareStatement(this.sql);
            this.preparedStatement.setInt(1,id);
            this.rs = this.preparedStatement.executeQuery();

            while(this.rs.next()){
                u.setId(this.rs.getInt("id_funcionario"));
                u.setNome(this.rs.getString("nome"));
                u.setCpf(this.rs.getString("cpf"));
                u.setTelefone(this.rs.getString("telefone"));
                u.setMatricula(this.rs.getString("matricula"));
            }
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
        return u;
    }

    public boolean editarFuncionario(Funcionario u){
        try {
            this.conn = new ConectaDB().getConexao();

            this.sql = "update funcionario SET nome = ?, cpf = ?, telefone=?, matricula=? WHERE id_funcionario = ?";
            this.preparedStatement = conn.prepareStatement(this.sql);
            this.preparedStatement.setString(1,u.getNome());
            this.preparedStatement.setString(2,u.getCpf());
            this.preparedStatement.setString(3,u.getTelefone());
            this.preparedStatement.setString(4,u.getMatricula());
            this.preparedStatement.setInt(5,u.getId());
            this.preparedStatement.execute();
        }catch(SQLException e){
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public boolean deletarFuncionario(int id){
        try {
            this.conn = new ConectaDB().getConexao();
            this.sql = "UPDATE funcionario SET ativo=false WHERE id_funcionario = ?";
            this.preparedStatement = conn.prepareStatement(this.sql);

            this.preparedStatement.setInt(1,id);
            this.preparedStatement.execute();

        }catch (SQLException e){
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public boolean cadastrarFuncionario(Funcionario u){
        try {
            this.conn = new ConectaDB().getConexao();
            this.sql = "insert into funcionario(nome, cpf, telefone, matricula, ativo) values (?,?,?,?,?)";

            this.preparedStatement = this.conn.prepareStatement(this.sql);
            this.preparedStatement.setString(1,u.getNome());
            this.preparedStatement.setString(2,u.getCpf());
            this.preparedStatement.setString(3,u.getTelefone());
            this.preparedStatement.setString(4,u.getMatricula());
            this.preparedStatement.setBoolean(5, true);
            this.preparedStatement.execute();
        } catch (SQLException e){
            e.printStackTrace();
            return false;
        }
        return true;
    }
}