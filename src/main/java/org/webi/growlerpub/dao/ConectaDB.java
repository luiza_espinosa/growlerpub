package org.webi.growlerpub.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.concurrent.ExecutionException;

public class ConectaDB {

    private final String URL = "jdbc:postgresql://localhost:5433/growler";
    private final String user = "postgres";
    private final String pw = "postgres";

    public Connection getConexao(){
        Connection conn = null;
        try{
            conn = DriverManager.getConnection(this.URL,this.user,this.pw);
        }catch(Exception e){
            e.printStackTrace();
        }
        return conn;
    }
}