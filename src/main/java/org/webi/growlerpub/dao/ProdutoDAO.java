package org.webi.growlerpub.dao;

import org.webi.growlerpub.model.Produto;

import java.sql.*;
import java.util.ArrayList;

public class ProdutoDAO {
    private String sql;
    private ResultSet rs;
    private PreparedStatement preparedStatement;
    private Connection conn;
    public boolean cadastrarProduto(Produto prod){
        try(Connection conn = new ConectaDB().getConexao()){
            this.sql = "INSERT INTO produto(nome, descricao) VALUES (?,?)";

            this.preparedStatement = conn.prepareStatement(this.sql);
            this.preparedStatement.setString(1,prod.getNome());
            this.preparedStatement.setString(2,prod.getDescricao());
            this.preparedStatement.execute();
        }catch (SQLException e){
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public ArrayList<Produto> buscarTodosProdutos(){
        ArrayList<Produto> roupas = new ArrayList<>();

        try(Connection conn = new ConectaDB().getConexao()){
            this.sql = "SELECT * FROM produto ORDER BY nome";
            this.preparedStatement = conn.prepareStatement(sql);
            this.rs = preparedStatement.executeQuery();
            while(rs.next()){
                Produto r = new Produto();
                r.setId(rs.getInt("id_produto"));
                r.setNome(rs.getString("nome"));
                r.setDescricao(rs.getString("descricao"));
                r.setValor(rs.getFloat("valor"));
                roupas.add(r);
            }
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
        return roupas;
    }

    public Produto buscaProduto(int id){
        Produto r = new Produto();

        try(Connection conn = new ConectaDB().getConexao()){
            this.sql = "SELECT * FROM produto WHERE id_produto = ?";

            this.preparedStatement = conn.prepareStatement(this.sql);
            this.preparedStatement.setInt(1,id);
            this.rs = this.preparedStatement.executeQuery();

            while (this.rs.next()){
                r.setId(this.rs.getInt("id_produto"));
                r.setNome(this.rs.getString("nome"));
                r.setDescricao(this.rs.getString("descricao"));
            }
        }catch (SQLException e){
            e.printStackTrace();
            return null;
        }
        return r;
    }
    public boolean editarProduto(Produto r){
        try(Connection conn = new ConectaDB().getConexao()){
            this.sql = "UPDATE produto SET nome = ?, descricao = ? WHERE id_produto = ?";
            this.preparedStatement = conn.prepareStatement(this.sql);
            this.preparedStatement.setString(1,r.getNome());
            this.preparedStatement.setString(2,r.getDescricao());
            this.preparedStatement.setInt(3,r.getId());
            this.preparedStatement.execute();
        }catch (SQLException e){
            e.printStackTrace();
            return false;
        }
        return true;
    }
}