package org.webi.growlerpub.dao;

import org.webi.growlerpub.model.Cliente;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ClienteDAO {
    private String sql;
    private ResultSet rs;
    private PreparedStatement preparedStatement;
    private Connection conn;

    public ArrayList<Cliente> buscarClientes () {
        ArrayList<Cliente> clientes = new ArrayList<>();
        try {
            this.conn = new ConectaDB().getConexao();
            this.sql = "SELECT id_cliente, nome, cpf, ativo FROM cliente;";
            this.preparedStatement = conn.prepareStatement(sql);
            this.rs = this.preparedStatement.executeQuery();
            while (this.rs.next()) {
                Cliente c = new Cliente();

                c.setId(this.rs.getInt("id_cliente"));
                c.setNome(this.rs.getString("nome"));
                c.setCpf(this.rs.getString("cpf"));
                c.setAtivo(this.rs.getBoolean("ativo"));
                clientes.add(c);
            }
        } catch (SQLException e){
            e.printStackTrace();
            return null;
        }
        return clientes;
    }

    public void cadastrarClientes(Cliente cliente) {
        try {
            this.conn = new ConectaDB().getConexao();
            this.sql = "INSERT INTO cliente VALUES (default, ?, ?, ?, ?, ?);";
            this.preparedStatement = conn.prepareStatement(sql);
            this.preparedStatement.setString(1, cliente.getNome());
            this.preparedStatement.setString(2, cliente.getCpf());
            this.preparedStatement.setString(3, cliente.getTelefone());
            this.preparedStatement.setString(4, cliente.getEmail());
            this.preparedStatement.setBoolean(5, true);
            this.preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public boolean buscarClientePorCpf(String cpf) {
        boolean tem=false;
        try {
            this.conn = new ConectaDB().getConexao();
            this.sql = "SELECT cpf FROM cliente WHERE cpf=?;";
            this.preparedStatement = this.conn.prepareStatement(sql);
            this.preparedStatement.setString(1, cpf);
            this.rs = this.preparedStatement.executeQuery();
            tem = this.rs.getString(1) != null;
        } catch (SQLException e){
            e.printStackTrace();
        }
        return tem;
    }

    public boolean editarCliente(Cliente cliente){
        try {
            this.conn = new ConectaDB().getConexao();
            this.sql = "UPDATE cliente SET nome=?, cpf=?, telefone=?, email=? WHERE id_cliente=?;";
            this.preparedStatement = this.conn.prepareStatement(sql);
            this.preparedStatement.setString(1, cliente.getNome());
            this.preparedStatement.setString(2, cliente.getCpf());
            this.preparedStatement.setString(3, cliente.getTelefone());
            this.preparedStatement.setString(4, cliente.getEmail());
            this.preparedStatement.setInt(5, cliente.getId());
            this.rs = this.preparedStatement.executeQuery();
        } catch (SQLException e){
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public Cliente buscarClientePeloId(int id_cliente) {
        Cliente cliente = new Cliente();
        try {
            this.conn = new ConectaDB().getConexao();
            this.sql = "SELECT * FROM cliente WHERE id_cliente=?;";
            this.preparedStatement = this.conn.prepareStatement(sql);
            this.preparedStatement.setInt(1, id_cliente);
            this.rs = this.preparedStatement.executeQuery();
            this.rs.next();

            cliente.setId(id_cliente);
            cliente.setCpf(this.rs.getString("cpf"));
            cliente.setNome(this.rs.getString("nome"));
            cliente.setEmail(this.rs.getString("email"));
            cliente.setTelefone(this.rs.getString("telefone"));
        } catch (SQLException e){
            e.printStackTrace();
            return null;
        }
        return cliente;
    }

    public void excluirCliente(int id_cliente) {
        try {
            this.conn = new ConectaDB().getConexao();
            this.sql = "UPDATE cliente SET ativo=false WHERE id_cliente=?;";
            this.preparedStatement = this.conn.prepareStatement(sql);
            this.preparedStatement.setInt(1, id_cliente);
            this.preparedStatement.execute();
        } catch (SQLException e){
            e.printStackTrace();
        }
    }
}
