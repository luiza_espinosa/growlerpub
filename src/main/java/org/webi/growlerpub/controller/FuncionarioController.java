package org.webi.growlerpub.controller;

import org.webi.growlerpub.model.Funcionario;
import org.webi.growlerpub.service.FuncionarioService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;

import java.util.ArrayList;

@Controller
@RequestMapping("/funcionario")
public class FuncionarioController {
    @GetMapping("/gerenciarFuncionarios")
    public String mostrarFuncionarios(Model model) {
        ArrayList<Funcionario> funcionarios = new FuncionarioService().buscarTodosFuncionarios();
        model.addAttribute("funcionarios", funcionarios);
        model.addAttribute("qtdFunc", funcionarios.size());
        return "jsp/funcionario/gerenciar-funcionarios";
    }
    @GetMapping("/cadastrarFuncionarios")
    public String cadastrarFuncionarios(Model model) {
        model.addAttribute("funcionarios", new Funcionario());
        return "jsp/funcionario/cadastrar-funcionario";
    }
    @GetMapping("/editar-func/{id}")
    public String editarFuncionarios(@PathVariable("id")int id_funcionario, Model model) {
        model.addAttribute("funcionarioE", new FuncionarioService().buscarFuncionarioPeloId(id_funcionario));
        return "jsp/funcionario/cadastrar-funcionario";
    }
    @GetMapping("/excluir-func/{id}")
    public String excluirClientes(@PathVariable("id")int id_funcionario, Model model) {
        model.addAttribute("funcionario", new FuncionarioService().buscarFuncionarioPeloId(id_funcionario));
        return "jsp/funcionario/excluir-funcionario";
    }
    @PostMapping("/cadastrar")
    public RedirectView efetuarCadastroFuncionario(@ModelAttribute("funcionario") Funcionario funcionario){
        new FuncionarioService().cadastraFuncionario(funcionario);
        return new RedirectView("/growler/funcionario/gerenciarFuncionarios");
    }
    @PostMapping("/editar")
    public RedirectView alterarCadastroFuncionario(@ModelAttribute("funcionario") Funcionario funcionario){
        new FuncionarioService().editarFuncionario(funcionario);
        return new RedirectView("/growler/funcionario/gerenciarFuncionarios");
    }
    @PostMapping("/excluir")
    public RedirectView deletarFuncionario(@ModelAttribute("id") Funcionario funcionario) {
        new FuncionarioService().excluirFuncionario(funcionario.getId());
        return new RedirectView("/growler/funcionario/gerenciarFuncionarios");
    }
}
