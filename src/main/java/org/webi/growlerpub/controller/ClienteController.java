package org.webi.growlerpub.controller;

import org.webi.growlerpub.model.Cliente;
import org.webi.growlerpub.service.ClienteService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;

import java.util.ArrayList;

@Controller
@RequestMapping("/cliente")
public class ClienteController {
    @GetMapping("/gerenciarClientes")
    public String mostrarClientes(Model model) {
        ArrayList<Cliente> clientes = new ClienteService().buscarClientes();
        model.addAttribute("clientes", clientes);
        model.addAttribute("qtdCli", clientes.size());
        return "jsp/cliente/gerenciar-clientes";
    }
    @GetMapping("/cadastrarClientes")
    public String cadastrarClientes(Model model) {
        model.addAttribute("cliente", new Cliente());
        return "jsp/cliente/cadastrar-cliente";
    }
    @GetMapping("/editar-cli/{id}")
    public String editarClientes(@PathVariable("id")int id_cliente, Model model) {
        model.addAttribute("clienteE", new ClienteService().buscarClientePorId(id_cliente));
        return "jsp/cliente/cadastrar-cliente";
    }
    @GetMapping("/excluir-cli/{id}")
    public String excluirClientes(@PathVariable("id")int id_cliente, Model model) {
        model.addAttribute("cliente", new ClienteService().buscarClientePorId(id_cliente));
        return "jsp/cliente/excluir-cliente";
    }
    @PostMapping("/cadastrar")
    public RedirectView efetuarCadastroCliente(@ModelAttribute("cliente")Cliente cliente){
        new ClienteService().cadastrarClientes(cliente);
        return new RedirectView("/growler/cliente/gerenciarClientes");
    }
    @PostMapping("/editar")
    public RedirectView alterarCadastroCliente(@ModelAttribute("cliente")Cliente cliente){
        new ClienteService().editarCliente(cliente);
        return new RedirectView("/growler/cliente/gerenciarClientes");
    }
    @PostMapping("/excluir")
    public RedirectView deletarCliente(@ModelAttribute("id")Cliente cliente) {
        new ClienteService().excluirCliente(cliente.getId());
        return new RedirectView("/growler/cliente/gerenciarClientes");
    }
}
