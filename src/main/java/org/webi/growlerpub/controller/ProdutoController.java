package org.webi.growlerpub.controller;

import org.webi.growlerpub.model.Produto;
import org.webi.growlerpub.service.ProdutosService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;

import java.util.ArrayList;

@Controller
@RequestMapping("/produto")
public class ProdutoController {
    @GetMapping("/gerenciarProdutos")
    public String mostrarProdutos(Model model) {
        ArrayList<Produto> produtos = new ProdutosService().buscarTodosProdutos();
        model.addAttribute("produtos", produtos);
        model.addAttribute("qtdProd", produtos.size());
        return "jsp/produto/gerenciar-produtos";
    }
    @GetMapping("/cadastrarProdutos")
    public String cadastrarProdutos(Model model) {
        model.addAttribute("produto", new Produto());
        return "jsp/produto/cadastrar-produtos";
    }
    @GetMapping("/editar-prod/{id}")
    public String editarProdutos(@PathVariable("id")int id_prod, Model model) {
        model.addAttribute("produtoE", new ProdutosService().buscarProdutoPeloId(id_prod));
        return "jsp/produto/cadastrar-produtos";
    }
    @PostMapping("/cadastrar")
    public RedirectView efetuarCadastroProduto(@ModelAttribute("produto") Produto produto){
        new ProdutosService().cadastrarProduto(produto);
        return new RedirectView("/growler/produto/gerenciarProdutos");
    }
    @PostMapping("/editar")
    public RedirectView alterarCadastroProduto(@ModelAttribute("produto") Produto produto){
        new ProdutosService().editarProduto(produto);
        return new RedirectView("/growler/produto/gerenciarProdutos");
    }
}
