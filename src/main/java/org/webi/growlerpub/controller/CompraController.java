package org.webi.growlerpub.controller;

import org.webi.growlerpub.dao.ProdutoDAO;
import org.webi.growlerpub.model.Cliente;
import org.webi.growlerpub.model.Compra;
import org.webi.growlerpub.model.Funcionario;
import org.webi.growlerpub.model.Produto;
import org.webi.growlerpub.service.ClienteService;
import org.webi.growlerpub.service.CompraService;
import org.webi.growlerpub.service.ProdutosService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;

@Controller
@RequestMapping("/compra")
public class CompraController {
    @GetMapping("/gerenciarCompras")
    public String mostrarCompras(Model model) {
        ArrayList<Compra> compras = new CompraService().todasAsCompras();
        model.addAttribute("compras", compras);
        model.addAttribute("qtdComp", compras.size());
        return "jsp/compra/gerenciar-compras";
    }
    @GetMapping("/cadastrarCompras")
    public String cadastrarCompra(Model model) {
        model.addAttribute("compra", new Compra());
        model.addAttribute("clientes", new ClienteService().buscarClientes());
        model.addAttribute("itens", new ProdutoDAO().buscarTodosProdutos());
        return "jsp/compra/cadastrar-compra";
    }
    @GetMapping("/editar-comp/{id}")
    public String editarCompra(@PathVariable("id")int id_compra, Model model) {
        model.addAttribute("compraE", new CompraService().buscarCompraPeloIdCompra(id_compra));
        model.addAttribute("itens", new ProdutosService().buscarTodosProdutos());
        return "jsp/compra/cadastrar-compra";
    }
    @GetMapping("/pagar-comp/{id}")
    public String pagarCompra(@PathVariable("id")int id_compra, Model model){
        model.addAttribute("compra", new CompraService().buscarCompraPeloIdCompra(id_compra));
        return "jsp/compra/pagar-comp";
    }
    @GetMapping("/ver-comp/{id}")
    public String verCompra(@PathVariable("id")int id_compra, Model model){
        model.addAttribute("compraE", new CompraService().buscarCompraPeloIdCompra(id_compra));
        return "jsp/compra/ver-comp";
    }
    @PostMapping("/cadastrar")
    public RedirectView efetuarCadastroCompra(HttpServletRequest request, HttpSession session){
        String id = request.getParameter("cliente.id");
        String[] items = request.getParameterValues("itemId[]");
        String[] quantidade = request.getParameterValues("quantidade[]");
        Compra compra = new Compra();
        Cliente cliente = new Cliente();
        int id_convertido = Integer.parseInt(id);
        cliente.setId(id_convertido);
        compra.setCliente(cliente);
        Funcionario funcionario = (Funcionario) session.getAttribute("func");
        compra.setFuncionario(funcionario);

        //ajeita lista de itens
        ArrayList<Produto> produtos = new ArrayList<>();
        for (String item : items) {
            Produto prod = new Produto();
            prod.setId(Integer.parseInt(item));
            produtos.add(prod);
        }
        for (int i=0; i < quantidade.length ; i++) {
            produtos.get(i).setQuantidade(Integer.parseInt(quantidade[i]));
        }
        compra.setItems(produtos);
        compra.setPago(false);
        new CompraService().cadastrarCompra(compra);
        return new RedirectView("/growler/compra/gerenciarCompras");
    }
    @PostMapping("/editar")
    public RedirectView editarCompra(HttpServletRequest request, HttpSession session){
        String id_compra = request.getParameter("compra.id");
        String[] items = request.getParameterValues("itemId[]");
        String[] quantidade = request.getParameterValues("quantidade[]");
        Compra compra = new Compra();

        int id_convertido = Integer.parseInt(id_compra);
        compra.setId(id_convertido);
        //ajeita lista de itens
        ArrayList<Produto> produtos = new ArrayList<>();
        for (String item : items) {
            Produto prod = new Produto();
            prod.setId(Integer.parseInt(item));
            produtos.add(prod);
        }
        for (int i=0; i < quantidade.length ; i++) {
            produtos.get(i).setQuantidade(Integer.parseInt(quantidade[i]));
        }
        compra.setItems(produtos);
        compra.setPago(false);
        new CompraService().editarCompra(compra);
        return new RedirectView("/growler/compra/gerenciarCompras");
    }
    @PostMapping("/pagar")
    public RedirectView pagarCompra(@ModelAttribute("id") Compra compra){
        new CompraService().efetuarPagamento(compra);
        return new RedirectView("/growler/compra/gerenciarCompras");
    }
}
