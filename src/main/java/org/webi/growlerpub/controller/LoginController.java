package org.webi.growlerpub.controller;

import org.webi.growlerpub.model.Funcionario;
import org.webi.growlerpub.service.FuncionarioService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpSession;

@Controller
public class LoginController {

    @PostMapping("/logar")
    public RedirectView logar(Model model, HttpSession session, @ModelAttribute("func") Funcionario usuarioLogando) {
        Funcionario u = new FuncionarioService().autenticar(usuarioLogando.getCpf(), usuarioLogando.getMatricula());
        if(u != null){
            session.setAttribute("func", u);
            return new RedirectView("/growler/home");
        }
        return new RedirectView("/growler");
    }

    @GetMapping("")
    public String mostrarIndex(){
        return "index";
    }
    @GetMapping("/logout")
    public String sair(HttpSession session){
        session.invalidate();
        return "index";
    }
}
