<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="pt-br">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" crossorigin="anonymous" rel="stylesheet">
  <title>Home</title>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="container">
    <a class="navbar-brand" href="#"><p style="font-family: 'Abyssinica SIL'">Growler</p></a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="navbar-nav ms-auto">
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            <i class="bi bi-person-circle"></i>${func.nome}
          </a>
          <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
            <li><a class="dropdown-item" href="/growler/logout">Sair</a></li>
          </ul>
        </li>
      </ul>
    </div>
  </div>
</nav>
<div class="container d-flex justify-content-center align-items-center text-center vh-100">
  <div class="card">
    <div class="card-body">
      <img src="<c:url value="/img/growler-icon.png"/>" alt="Logo" class="img-fluid mb-4">
      <h2 class="card-title">Cadastro de Clientes</h2>
      <br>
      <br>
      <div class="btn-group" role="group" aria-label="Crud">
        <a href="/growler/cliente/gerenciarClientes" class="btn btn-outline-dark">Vizualizar</a>
        <a href="/growler/cliente/cadastrarClientes" class="btn btn-dark">Cadastrar</a>
      </div>
      <br>
      <br>
      <div>
        <c:if test="${clienteE == null}">
          <form:form method="post" action="/growler/cliente/cadastrar" modelAttribute="cliente">
            <div class="form-group">
              <label for="nome">Nome:</label>
              <input required type="text" id="nome" class="form-control" name="nome">
            </div>
            <div class="form-group">
              <label for="cpf">CPF:</label>
              <input required type="text" id="cpf" class="form-control" placeholder="000.000.000-00" name="cpf">
            </div>
            <div class="form-group">
              <label for="telefone">Telefone:</label>
              <input required type="text" id="telefone" class="form-control" placeholder="(99)99999-9999" name="telefone">
            </div>
            <div class="form-group">
              <label for="email">Email:</label>
              <input type="email" id="email" class="form-control" placeholder="fulano@gmail.com" name="email">
            </div>
            <div class="form-group">
              <br>
              <button type="submit" class="btn btn-outline-success">Cadastrar</button>
            </div>
          </form:form>
        </c:if>
        <c:if test="${clienteE != null}">
          <form:form method="post" action="/growler/cliente/editar" modelAttribute="cliente">
            <div class="form-group">
              <label for="nome">Nome:</label>
              <input required type="text" id="nome" class="form-control" name="nome" value="${clienteE.nome}">
            </div>
            <div class="form-group">
              <label for="cpf">CPF:</label>
              <input required type="text" id="cpf" class="form-control" placeholder="000.000.000-00" name="cpf" value="${clienteE.cpf}">
            </div>
            <div class="form-group">
              <label for="telefone">Telefone:</label>
              <input required type="text" id="telefone" class="form-control" placeholder="(99)99999-9999" name="telefone" value="${clienteE.telefone}">
            </div>
            <div class="form-group">
              <label for="email">Email:</label>
              <input type="email" id="email" class="form-control" placeholder="fulano@gmail.com" name="email" value="${clienteE.email}">
            </div>
            <input type="hidden" id="id" name="id" value="${clienteE.id}">
            <div class="form-group">
              <br>
              <button type="submit" class="btn btn-outline-success">Editar</button>
            </div>
          </form:form>
        </c:if>

      </div>
    </div>
  </div>
</div>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js"></script>
</body>
</html>