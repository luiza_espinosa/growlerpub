<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="pt-br">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" crossorigin="anonymous" rel="stylesheet">
  <title>Home</title>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="container">
    <a class="navbar-brand" href="/growler/home/"><p style="font-family: 'Abyssinica SIL'">Growler</p></a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="navbar-nav ms-auto">
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            <i class="bi bi-person-circle"></i>${func.nome}
          </a>
          <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
            <li><a class="dropdown-item" href="/growler/logout">Sair</a></li>
          </ul>
        </li>
      </ul>
    </div>
  </div>
</nav>
<div class="container mt-5 d-flex justify-content-center align-items-center text-center vh-100">
  <div class="card mt-5">
    <div class="card-body">
      <img src="<c:url value="/img/growler-icon.png"/>" alt="Logo" class="img-fluid mb-4">
      <h2 class="card-title">Cadastro de Compra</h2>
      <br>
      <br>
      <div class="btn-group" role="group" aria-label="Crud">
        <a href="/growler/compra/gerenciarCompras" class="btn btn-outline-dark">Visualizar</a>
        <a href="/growler/compra/cadastrarCompras" class="btn btn-dark">Cadastrar</a>
      </div>
      <br>
      <br>
      <div>
        <c:if test="${compraE == null}">
          <form:form method="post" action="/growler/compra/cadastrar" modelAttribute="compra">
            <div class="form-group">
              <label for="cliente.id">Selecione o Cliente:</label>
              <select required class="form-select" id="cliente.id" name="cliente.id" aria-label="Default select example">
                <option selected>Selecione um cliente...</option>
                <c:forEach items="${clientes}" var="cli">
                  <option value="${cli.id}">${cli.nome} - ${cli.cpf}</option>
                </c:forEach>
              </select>
            </div>
            <div class="form-group">
              <br>
              <p>Selecione os items: </p>
              <div id="items" class="overflow-auto" style="max-height: 300px">
                <div class="item-row">
                  <div class="form-row">
                    <div class="col-8">
                      <select required class="form-select item-select" name="itemId[]" aria-label="Default select example">
                        <option selected>Selecione o item</option>
                        <c:forEach items="${itens}" var="item">
                          <option value="${item.id}">${item.nome} - R$${item.valor}</option>
                        </c:forEach>
                      </select>
                    </div>
                    <br>
                    <div class="col-2">
                      <input required type="number" class="form-control quantidade-input" name="quantidade[]" value="1" step="1" min="1" placeholder="Quantidade">
                    </div>
                    <br>
                    <div class="col-2">
                      <button type="button" class="btn btn-outline-danger" onclick="removeItem(this)">Remover</button>
                    </div>
                  </div>
                  <br>
                </div>
              </div>
              <button type="button" class="btn btn-outline-primary" onclick="addItem()">Adicionar Item</button>
            </div>
            <input type="hidden" value="${func.id}" name="funcionario.id">
            <div class="form-group">
              <br>
              <button type="submit" class="btn btn-outline-success">Cadastrar Compra</button>
            </div>
          </form:form>
        </c:if>
        <c:if test="${compraE != null}">
          <form:form method="post" action="/growler/compra/editar" modelAttribute="compra">
            <div class="form-group">
              <label for="cliente.id">Selecione o Cliente:</label>
              <select required class="form-select" id="cliente.id" name="cliente.id" aria-label="Default select example">
                <option value="${compraE.cliente.id}" selected>${compraE.cliente.nome} - ${compraE.cliente.cpf}</option>
              </select>
            </div>
            <div class="form-group">
              <br>
              <p>Selecione os items: </p>
              <div id="items" class="overflow-auto" style="max-height: 300px">
                <c:forEach items="${compraE.items}" var="item">
                  <div class="item-row">
                    <div class="form-row">
                      <div class="col-8">
                        <select required class="form-select item-select" name="itemId[]" aria-label="Default select example">
                          <option value="${item.id}" selected>${item.nome} - R$ ${item.valor}</option>
                        </select>
                      </div>
                      <br>
                      <div class="col-2">
                        <input required type="number" class="form-control quantidade-input" name="quantidade[]" value="${item.quantidade}" step="1" min="1" placeholder="Quantidade">
                      </div>
                      <br>
                      <div class="col-2">
                        <button type="button" class="btn btn-outline-danger" onclick="removeItem(this)">Remover</button>
                      </div>
                    </div>
                    <br>
                  </div>
                </c:forEach>
              </div>
              <button type="button" class="btn btn-outline-primary" onclick="addItemEditar()">Adicionar Item</button>
            </div>
            <div class="form-group">
              <label for="funcionario.nome">Func. que efetuou a compra:</label>
              <input readonly type="text" value="${compraE.funcionario.nome}" id="funcionario.nome" class="form-control" name="funcionario.nome">
            </div>
            <input type="hidden" value="${compraE.funcionario.id}" name="funcionario.id">
            <input type="hidden" value="${compraE.id}" name="compra.id">
            <div class="form-group">
              <br>
              <button type="submit" class="btn btn-outline-success">Editar Compra</button>
            </div>
          </form:form>
        </c:if>
      </div>
    </div>
  </div>
</div>

<script>
  function addItem() {
    var itemRow = document.querySelector('.item-row:last-of-type');
    var newItemRow = itemRow.cloneNode(true);

    newItemRow.querySelector('.item-select').selectedIndex = 0;
    newItemRow.querySelector('.quantidade-input').value = '';
    itemRow.parentNode.appendChild(newItemRow);
  }

  function addItemEditar() {
    var itemRow = document.querySelector('.item-row:last-of-type');
    var newItemRow = itemRow.cloneNode(true);
    var itemSelect = newItemRow.querySelector('.item-select');
    itemSelect.innerHTML = '';

    <c:forEach items="${itens}" var="it">
      var newOp${it.id} = document.createElement("option");
      newOp${it.id}.text = "${it.nome} - R$ ${it.valor}";
      newOp${it.id}.value = ${it.id};

      itemSelect.add(newOp${it.id});
    </c:forEach>
    newItemRow.querySelector('.item-select').selectedIndex = 0;
    newItemRow.querySelector('.quantidade-input').value = '';
    itemRow.parentNode.appendChild(newItemRow);
  }

  function removeItem(button) {
    var itemRow = button.closest('.item-row');
    if (itemRow.parentNode.childElementCount > 1) {
      itemRow.remove();
    }
  }
</script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js"></script>
</body>
</html>
