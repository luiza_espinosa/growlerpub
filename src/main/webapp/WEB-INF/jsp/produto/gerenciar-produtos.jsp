<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" crossorigin="anonymous" rel="stylesheet">
    <title>Home</title>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container">
        <a class="navbar-brand" href="/growler/home"><p style="font-family: 'Abyssinica SIL'">Growler</p></a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav ms-auto">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        <i class="bi bi-person-circle"></i>${func.nome}
                    </a>
                    <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                        <li><a class="dropdown-item" href="/growler/logout">Sair</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>
<div class="container mt-5 d-flex justify-content-center align-items-center text-center vh-100">
    <div class="card mt-5 ">
        <div class="card-body">
            <img src="<c:url value="/img/growler-icon.png"/>" alt="Logo" class="img-fluid mb-4">
            <h2 class="card-title">Lista de Produtos</h2>
            <br>
            <br>
            <div class="btn-group" role="group" aria-label="Crud">
                <a href="/growler/produto/gerenciarProdutos" class="btn btn-dark">Vizualizar</a>
                <a href="/growler/produto/cadastrarProdutos" class="btn btn-outline-dark">Cadastrar</a>
            </div>
            <br>
            <br>
            <div class="overflow-auto" style="max-height: 300px">
                <table class="table table-striped">
                    <thead>
                    <tr class="sticky-top">
                        <th scope="col">Nome</th>
                        <th scope="col">Descrição</th>
                        <th scope="col">Ações</th>
                    </tr>
                    </thead>
                    <tbody>

                            <c:forEach items="${produtos}" var="prod">
                                <tr>
                                    <td>${prod.nome}</td>
                                    <td>${prod.descricao}</td>
                                    <td>
                                        <div class="btn-group" role="group" aria-label="Ações">
                                            <a href="/growler/produto/editar-prod/${prod.id}" class="btn btn-outline-dark">Editar</a>
                                        </div>
                                    </td>
                                </tr>
                            </c:forEach>

                    </tbody>
                </table>
            </div>
            <br>
            <h2 class="card-title">Total de Produtos: ${qtdProd}</h2>
        </div>
    </div>
</div>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js"></script>
</body>
</html>