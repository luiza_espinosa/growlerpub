<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="pt-br">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" crossorigin="anonymous" rel="stylesheet">
  <title>Login</title>
</head>
<body>
<div class="container d-flex justify-content-center align-items-center vh-100">
  <div class="card">
    <div class="card-body">
      <h2 class="card-title">Tela de Login</h2>
      <form:form method="post" action="/growler/logar" modelAttribute="func">
        <div class="form-group">
          <label for="cpf">CPF:</label>
          <input required type="text" id="cpf" class="form-control" name="cpf">
        </div>
        <br>
        <div class="form-group">
          <label for="matricula">Matricula:</label>
          <input required type="text" id="matricula" class="form-control" minlength="3" maxlength="10" name="matricula">
        </div>
        <br>
        <br>
        <div class="form-group">
          <button type="submit" class="btn btn-outline-dark">Logar</button>
        </div>
      </form:form>
    </div>
  </div>
</div>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js"></script>
</body>
</html>